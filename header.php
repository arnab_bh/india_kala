<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Welcome to India Kala</title>
<!-- Responsive Meta Tag -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- main stylesheet -->
<link rel="stylesheet" href="css/style.css"/>
<link rel="stylesheet" href="css/mega-menu.css"/>
<link rel="stylesheet" href="css/login.css"/>
<link rel="stylesheet" href="css/cart-dropdown.css"/>

<!-- responsive stylesheet -->
<link rel="stylesheet" href="css/responsive.css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/jquery-ui.css"/>

<script src="js/jquery.min.js"></script> <!-- jQuery JS --> 
<script src="js/bootstrap.min.js"></script> <!-- BootStrap JS -->
<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
<!--[if lt IE 9]>
		<script src="js/respond.js"></script>
	<![endif]-->
</head>
<body>
<!-- header -->
<header> 
  <!---------------top black header start ---------------------------------------->
  <div class="top-nav">
    <div class="container">
      <div class="row">
        <div class="col-sm-5 top-iconR"><div class="searchR"><input type="text" /></div></div>
        <div class="col-sm-7">
          <div class="top-navR">
            <!--<div class="top-drop">
              <div class="btn-group show-on-hover">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> ENGLISH 
                <span class="fa fa-chevron-down"></span> </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Lorem 1</a></li>
                  <li><a href="#">Lorem 2</a></li>
                  <li><a href="#">Lorem 3</a></li>
                  <li><a href="#">Lorem 4</a></li>
                  <li><a href="#">Lorem 5</a></li>
                  <li><a href="#">Lorem 6</a></li>
                </ul>
              </div>
            </div>-->
            <div class="top-drop">
              <div class="btn-group show-on-hover">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> INR 
                <span class="fa fa-chevron-down"></span> </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Lorem 1</a></li>
                  <li><a href="#">Lorem 2</a></li>
                  <li><a href="#">Lorem 3</a></li>
                  <li><a href="#">Lorem 4</a></li>
                </ul>
              </div>
            </div>
            <div class="top-drop">
              <div class="btn-group show-on-hover">
                <button type="button" class="btn btn-default dropdown-toggle" onclick="window.location.href='login-signup.php'"> 
                LOG IN &nbsp; OR &nbsp; REGISTER
               <!-- <span class="fa fa-chevron-down"></span>--> </button>
                <!--<ul class="dropdown-menu" role="menu">
                  <li><a href="#">Lorem 1</a></li>
                  <li><a href="#">Lorem 2</a></li>
                  <li><a href="#">Lorem 3</a></li>
                  <li><a href="#">Lorem 4</a></li>
                  <li><a href="#">Lorem 5</a></li>
                  <li><a href="#">Lorem 6</a></li>
                </ul>-->
              </div>
            </div>
            <div class="top-iconR">
              <!--<div class="searchR"><a href="#"><i class="fa fa-search"></i></a></div>-->
              <div class="bagR">
              
              <div class="btn-group show-on-hover">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-suitcase"></i> <span>2</span></button>
                <ul class="dropdown-menu dropdown-cart" role="menu">
              <li>
                  <div class="item">
                    <div class="item-left">
                        <a href="product-details.php"><img src="img/order-img.jpg" alt="" class="pop-cart"/>
                        <div class="item-info">
                            <p class="p-name"><a href="product-details.php">SHETLAND WOOL FULL - ZIP SWEATER</a></p>
                            <p class="p-price">Rs. 11,000/-</p>
                        </div>
                    </div>
                    <div class="item-right">
                        <a href="javascript:(0)">x</a>
                    </div>
                </div>
              </li>
              <li>
                  <div class="item">
                    <div class="item-left">
                        <a href="product-details.php"><img src="img/order-img.jpg" alt="" class="pop-cart"/></a>
                        <div class="item-info">
                            <p class="p-name"><a href="product-details.php">SHETLAND WOOL FULL</a></p>
                            <p class="p-price">Rs. 12,000/-</p>
                        </div>
                    </div>
                    <div class="item-right">
                       <a href="javascript:(0)">x</a>
                    </div>
                </div>
              </li>
              <li class="divider"></li>
              <li><a class="view-cart" href="cart.php">View Cart</a></li>
          </ul>
              </div>
              
              
              </div>
              <div class="likeR"><a href="#"><i class="fa fa-heart"></i></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!---------------top black header start ---------------------------------------->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 logo"> <a href="index.php"> <img src="img/logo.png" alt="India Kala"> </a> </div>
    </div>
  </div>
  
  <div class="container full-menubar">
    <nav class="navbar navbar-inverse">
      <div class="navbar-header">
      <div class="hidden-sm hidden-md" hidden-lg><span class="m-text">MENU</span></div>
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse"> 
        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> 
        <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>
      <div class="collapse navbar-collapse js-navbar-collapse DesktopR0">
        <ul class="nav navbar-nav">
          <li class="dropdown mega-dropdown"> <a href="javascript:void(0)" class="dropdown-toggle border-active" data-toggle="dropdown">Saree</a>
            <ul class="dropdown-menu mega-dropdown-menu">
              <li class="col-sm-3 drop-width">
                <div class="drop-01"></div>
                <div class="img-desc">
                  <h4>Paithni Bedsheet</h4>
                  <p>Be inspired by our new age dummy some dummy details</p>
                </div>
              </li>
              <li class="col-sm-3 drop-width">
                <div class="drop-01"></div>
                <div class="img-desc">
                  <h4>Paithni Bedsheet</h4>
                  <p>Be inspired by our new age dummy some dummy details</p>
                </div>
              </li>
              <li class="col-sm-3 drop-width">
                <div class="drop-01"></div>
                <div class="img-desc">
                  <h4>Paithni Bedsheet</h4>
                  <p>Be inspired by our new age dummy some dummy details</p>
                </div>
              </li>
              <li class="col-sm-3 drop-widthR">
                <h4>Shop by Collection</h4>
                <ul>
                  <li>Category 1</li>
                  <li>Category 2</li>
                  <li>Category 3</li>
                  <li>Category 4</li>
                  <li>Category 5</li>
                  <li>Category 6</li>
                  <li>Category 6</li>
                  <li>Category 7</li>
                  <li>Category 8</li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="javascript:void(0)">Apparel</a></li>
          <li><a href="javascript:void(0)">Accessories</a></li>
          <li><a href="javascript:void(0)">Home Decor</a></li>
          <li><a href="javascript:void(0)">Pure</a></li>
          <li><a href="javascript:void(0)">Ivory Room</a></li>
          <!--<li><a href="blog.php">BLOG</a></li>-->
          <li class="dropdown last-dropdown"><a href="javascript:void(0)" style="color:#00afa9;">Search By Art</a>
          	<ul class="dropdown-menu">
                <li><a href="javascript:void(0)">Art 1</a></li>
                <li><a href="javascript:void(0)">Art 2</a></li>
                <li><a href="javascript:void(0)">Art 3</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!-- /.nav-collapse --> 
    </nav>
  </div>
  <div class="clearfix"></div>
</header>
<!-- /header -->