<?php include('header.php');?>
<link rel="stylesheet" type="text/css" href="css/payment.css" >
<link rel="stylesheet" type="text/css" href="css/payment_option.css" >

<section class="content_part">
	<article>
  	<div class="container container-details">
        <div class="row"> 
          <!-- Breadcrumb Column -->
          <div class="col-xs-12">
            <ol class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">Payment</li>
            </ol>
          </div>
          <!-- End Column -->
        </div>
    </div>  
  </article>
  <article class="full-black-bg">
    <div class="container payment_container">
      <div class="row">
          <div class="col-xs-12">
            <h4 class="head_text">STEP 3 : PAYMENT OPTION</h4>
          </div>
      </div>
    </div>
  </article>
	<article>
  		<div class="container payment_container">
            <div class="col-sm-12 order-mode-area">
            <div class="row">
            <div class="col-sm-12">
            <h5>CHOOSE PAYMENT MODE</h5>
            <small>Convenience fee will be charged based on payment mode</small>
            </div>  
            </div>  
            
            <div class="row">
                    <div class="col-sm-3 tabL-width paddingLR0"> 
                      <ul class="nav nav-tabs tabs-left">
                        <li class="active"><a href="#credit-card" data-toggle="tab"><i class="fa fa-suitcase"></i> Credit Card </a></li>
                        <li><a href="#debit-card" data-toggle="tab"><i class="fa fa-suitcase"></i> Debit Card</a></li>
                        <li><a href="#net-banking" data-toggle="tab"><i class="fa fa-suitcase"></i> Net Banking</a></li>
                        <li><a href="#more-option" data-toggle="tab"><i class="fa fa-suitcase"></i> More Option</a></li>
                      </ul>
                    </div>
            
                    <div class="col-sm-9 tabR-width  paddingLR0">
                      <div class="tab-content">
                        <div class="tab-pane active" id="credit-card">
                        	<div class="form_part">
                                <label class="col-sm-4">Select Your Card</label>
                                <select name="Card" class="col-sm-4">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form_part">
                                <label class="col-sm-4 col-xs-12">Card Number</label>
                                <input type="text" placeholder="Example 124563987"  class="col-sm-4 col-xs-12" />
                                <a href="javascript void(0)">Manage this card</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form_part">
                                <label class="col-sm-4 col-xs-12">Name on Card </label>
                                <input type="text" placeholder="Ram Charan"  class="col-sm-4 col-xs-12" />
                                <div class="clearfix"></div>
                            </div>
                            <div class="form_part">
                                <label class="col-sm-4 col-xs-12">CVV Number</label>
                                <input type="text" placeholder="3 digit in card"  class="col-sm-4 col-xs-12" />
                                <a href="javascript void(0)">What is CVV Number?</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form_part">
                                <label class="col-sm-4 col-xs-12">Expiry Date</label>
                                <select name="Card" class="col-sm-1 col-xs-6" style="width:15.5%;">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <select name="Card" class="col-sm-1 col-xs-12" style="margin-left:15px;width:15.5%;">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <div class="clearfix"></div>
                            </div>
                            <div class="note">You might be redirected to partner site to verify your credentials before we proceed to authorize your payment.</div>
                            <div class="pay text-center"><a href="thank-you.php" class="red-btn">Pay Now</a></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-pane" id="debit-card">Lorem Ipsum A</div>
                        <div class="tab-pane" id="debit-atm">Lorem Ipsum B</div>
                        <div class="tab-pane" id="net-banking">Lorem Ipsum C</div>
                        <div class="tab-pane" id="more-option">Lorem Ipsum D</div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
            
                    <div class="clearfix"></div>
            
            
                  
                </div>    

            <div class="clearfix"></div> 
             
            </div>
		</div>
    </article>
</section>

<!-- footer Part Added-->
<?php include('footer.php');?> 