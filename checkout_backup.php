<?php include('header.php');?>
<section class="content_part">
<!-- # product details top start-->
<section id="product-details">
  <div class="container container-details">
    <div class="row"> 
      <!-- Breadcrumb Column -->
      <div class="col-xs-12">
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li class="active">Checkout</li>
        </ol>
      </div>
      <!-- End Column -->
      <div class="col-xs-12 checkout-acc">
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> 
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> 1. &nbsp; checkout option </a> </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
              <div class="panel-body login-area">
                <div class="col-sm-6">
                  <h4>NEW CUSTOMER</h4>
                  <p>Register with us for future convenience :</p>
                  <div class="filters">
                    <label>
                      <input type="checkbox">
                      <span class="icon"><i class="fa fa-check"></i></span> Checkout as Guest </label>
                  </div>
                  <div class="filters">
                    <label>
                      <input type="checkbox">
                      <span class="icon"><i class="fa fa-check"></i></span> Register </label>
                  </div>
                  <p class="log-desc">By creating an account with our store, you will be able to move through the checkout process faster, store 
                  multiple shipping address, view and track your orders in your account and more.</p>
                  <a href="#" class="red-btn">Continue</a> </div>
                <div class="col-sm-6">
                  <h4>REGISTERED CUSTOMERS</h4>
                  <p>Register with us for future convenience :</p>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR E-MAIL*" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR PASSWORD*" name="" class="custome-input"/>
                  </div>
                  <div class="filters">
                    <label>
                      <input type="checkbox">
                      <span class="icon"><i class="fa fa-check"></i></span> Remember password </label>
                  </div>
                  <a href="#" class="red-btn">Login</a> </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> 
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> 2. &nbsp; billing information </a> </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse in">
              <div class="panel-body login-area">
                <div class="col-sm-6">
                  <h4>YOUR PERSONAL DETAILS</h4>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR FIRST NAME *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR LAST NAME *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR EMAIL *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR TELEPHONE *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR POST FAX *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR COMPANY *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR PASSWORD *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR CONFIRM PASSWORD *" name="" class="custome-input"/>
                  </div>
                  <div class="filters">
                    <label>
                      <input type="checkbox">
                      <span class="icon"><i class="fa fa-check"></i></span> I wish to subscribe to the Vigo Shop newsletter. </label>
                  </div>
                  <div class="filters">
                    <label>
                      <input type="checkbox">
                      <span class="icon"><i class="fa fa-check"></i></span> My delivery and billing address are the same. </label>
                  </div>
                </div>
                <div class="col-sm-6">
                  <h4>YOUR ADDRESS</h4>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR ADDRESS*" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR ADDRESS " name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR CITY *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR POSTCODE *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <select name="" class="custome-input">
                      <option>ENTER YOUR POST COUNTRY *</option>
                      <option>India</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <select name="" class="custome-input">
                      <option>ENTER YOUR POST REGION/STATE * </option>
                      <option>India</option>
                    </select>
                  </div>
                  <div class="filters">
                    <label>
                      <input type="checkbox">
                      <span class="icon"><i class="fa fa-check"></i></span> I have reed and agree to the Privacy Policy. </label>
                  </div>
                  <a href="#" class="red-btn">Continue</a> </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> 
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> 3. &nbsp; delivery details </a> </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
              <div class="panel-body login-area">
                <div class="col-sm-6">
                  <h4>Lorem ipsum text</h4>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR PASSWORD *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR CONFIRM PASSWORD *" name="" class="custome-input"/>
                  </div>
                </div>
                <div class="col-sm-6">
                  <h4>Lorem ipsum</h4>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR ADDRESS*" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR ADDRESS " name="" class="custome-input"/>
                  </div>
                  <a href="#" class="red-btn">Continue</a> </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> 
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour"> 4. &nbsp; delivery method </a> </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
              <div class="panel-body login-area">
                <div class="col-sm-6">
                  <h4>Lorem ipsum text AB</h4>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR PASSWORD *" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR CONFIRM PASSWORD *" name="" class="custome-input"/>
                  </div>
                </div>
                <div class="col-sm-6">
                  <h4>Lorem ipsum 2</h4>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR ADDRESS*" name="" class="custome-input"/>
                  </div>
                  <div class="form-group">
                    <input type="text" placeholder="ENTER YOUR ADDRESS " name="" class="custome-input"/>
                  </div>
                  <a href="#" class="red-btn">Continue</a> </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> 
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFive"> 5. &nbsp; payment method </a> </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
              <div class="panel-body login-area">
                <div class="col-sm-12">
                  <h4>No payment method Available</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> 
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseSix"> 6. &nbsp; confirm order </a> </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
              <div class="panel-body" style="padding-left:0; padding-right:0; padding-top:0;">
                <div class="col-xs-12 paddingLR0">
                  <div class="table-responsive check-table">
                    <table class="table table-bordered" style="border-left:0;">
                      <thead>
                        <tr>
                          <th class="white-bg">&nbsp;</th>
                          <th class="white-bg">PRODUCT NAME</th>
                          <th class="white-bg">PRODUCT CODE</th>
                          <th class="white-bg">UNIT PRICE</th>
                          <th class="white-bg">QUANTITY</th>
                          <th class="white-bg">SUBTOTAL</th>
                          <th class="white-bg"><div class="remove-round">x</div></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th style="border-right:0; padding-left:16px;" scope="row"><img src="img/cart-img-01.jpg" alt=""/></th>
                          <td style="border-left:0;"><p class="pro-name">Black and white dust <br/>
                              Sweater dress</p>
                            <p class="pro-size">Color: black <br/>
                              Size: XS</p></td>
                          <td>MP125984154</td>
                          <td><span class="over-line">$321</span> $198</td>
                          <td><input type="text" name="" placeholder="1" class="input-cart"></td>
                          <td><span class="cart-tp">$198</span></td>
                          <td><a href="#" class="cart-remove">x</a></td>
                        </tr>
                        <tr>
                          <th style="border-right:0; padding-left:16px;" scope="row"><img src="img/cart-img-02.jpg" alt=""/></th>
                          <td style="border-left:0;"><p class="pro-name">Black and white dust <br/>
                              Sweater dress</p>
                            <p class="pro-size">Color: black <br/>
                              Size: XS</p></td>
                          <td>MP125984154</td>
                          <td><span class="over-line">$321</span> $198</td>
                          <td><input type="text" name="" placeholder="1" class="input-cart"></td>
                          <td><span class="cart-tp">$215</span></td>
                          <td><a href="#" class="cart-remove">x</a></td>
                        </tr>
                        <tr>
                          <th style="border-right:0; padding-left:16px;" scope="row"><img src="img/cart-img-03.jpg" alt=""/></th>
                          <td style="border-left:0;"><p class="pro-name">Black and white dust <br/>
                              Sweater dress</p>
                            <p class="pro-size">Color: black <br/>
                              Size: XS</p></td>
                          <td>MP125984154</td>
                          <td><span class="over-line">$321</span> $198</td>
                          <td><input type="text" name="" placeholder="1" class="input-cart"></td>
                          <td><span class="cart-tp">$324</span></td>
                          <td><a href="#" class="cart-remove">x</a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-sm-5 pull-right">
                  <div class="table-responsive cart-total">
                    <table class="table table-bordered">
                      <tr>
                        <td>SUBTOTAL :</td>
                        <td style="border-right:none;">$737</td>
                      </tr>
                      <tr>
                        <td>SHIPPING : </td>
                        <td style="border-right:none;">$6</td>
                      </tr>
                      <tr>
                        <td>TAX(5%) :</td>
                        <td style="border-right:none;">$37</td>
                      </tr>
                      <tr>
                        <td>TOTAL :</td>
                        <td style="border-right:none;">$780</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-xs-12"> <a href="index.php" class="conf-btn">CONFIRM ORDER</a> </div>
      <!-- End Column --> 
    </div>
  </div>
</section>
<!-- /# product details top end --> 
</section>
<!-- footer Part Added-->
<?php include('footer.php');?>