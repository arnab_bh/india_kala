
<footer>
  <section class="upper-bar">
  	<div class="container">
    <div class="row"> 
      <!-- .widget -->
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 widget">
        <h3>customer service</h3>
        <ul>
          <li><a href="#">contact us</a></li>
          <li><a href="#">faqs</a></li>
          <li><a href="#">delivery and returns</a></li>
          <li><a href="#">legal</a></li>
          <li><a href="#">site map</a></li>
        </ul>
      </div>
      <!-- /.widget --> 
      <!-- .widget -->
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 widget">
        <h3>about us</h3>
        <ul>
          <li><a href="#">our history</a></li>
          <li><a href="#">the brand</a></li>
          <li><a href="#">join us</a></li>
          <li><a href="#">popular searches</a></li>
        </ul>
      </div>
      <!-- /.widget --> 
      <!-- .widget -->
      <!--<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6 widget">
        <h3>cards &amp; discounts</h3>
        <ul>
          <li><a href="#">students</a></li>
          <li><a href="#">gift card</a></li>
          <li><a href="#">warehouse card</a></li>
        </ul>
      </div>-->
      <!-- /.widget --> 
      <!-- .widget -->
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 widget">
        <input type="text" name="signup for treats" value="sign up for treats">
        <input type="submit" value="Go" class="sign_btn">
        <ul class="social">
          <li><a href="#" class="hvr-radial-out"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#" class="hvr-radial-out"><i class="fa fa-instagram"></i></a></li>
          <li><a href="#" class="hvr-radial-out"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#" class="hvr-radial-out"><i class="fa fa-pinterest"></i></a></li>
          <li><a href="#" class="hvr-radial-out"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="#" class="hvr-radial-out"><i class="fa fa-youtube"></i></a></li>
          <li>&nbsp;</li>
        </ul>
      </div>
      <!-- /.widget --> 
    </div>
  </div>
  </section>
  <!-- #bottom-bar -->
	<section id="bottom-bar">
  <div class="container">
    <div class="row">
        <div class="col-sm-12 text-center">
          <ul>
              <li>IndiaKala &copy;2014 All rights reserved</li>
              <li>|</li>
              <li>Powered by <a href="http://www.bluehorse.in" target="_blank" class="atoz">Bluehorse</a></li>
          </ul>
        </div>
    </div>
  </div>
</section>
<!-- /#bottom-bar --> 
</footer>
<!-- /footer --> 


<script src="js/isotope.pkgd.min.js"></script> <!-- iSotope JS --> 
<script src="js/wow.js"></script> <!-- WOW JS -->  
<script src="js/easyzoom.js"></script>
<script src="js/custom.js"></script> <!-- Custom JS -->
<script src="js/jquery-ui.js"></script> 

<script>
		// Instantiate EasyZoom instances
		var $easyzoom = $('.easyzoom').easyZoom();

		// Setup thumbnails example
		var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

		$('.thumbnails').on('click', 'a', function(e) {
			var $this = $(this);

			e.preventDefault();

			// Use EasyZoom's `swap` method
			api1.swap($this.data('standard'), $this.attr('href'));
		});

		// Setup toggles example
		var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

		$('.toggle').on('click', function() {
			var $this = $(this);

			if ($this.data("active") === true) {
				$this.text("Switch on").data("active", false);
				api2.teardown();
			} else {
				$this.text("Switch off").data("active", true);
				api2._init();
			}
		});
	</script>
<script>
	$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
});	
	</script>
    <script>
  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 200,
      values: [0, 200 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "Rs." + ui.values[ 0 ] + " - Rs." + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "Rs." + $( "#slider-range" ).slider( "values", 0 ) +
      " - Rs." + $( "#slider-range" ).slider( "values", 1 ) );
  });
  </script>

</body>
</html>