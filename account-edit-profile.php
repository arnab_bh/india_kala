<?php include('header.php');?>
<link href="css/my-account.css" rel="stylesheet"/>
<section class="content_part">
<article>
  	<div class="container account_container">
        <div class="row"> 
          <!-- Breadcrumb Column -->
          <div class="col-xs-12">
            <ol class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">My Account</li>
            </ol>
          </div>
          <!-- End Column -->
        </div>
    </div>  
  </article>
<article class="full-black-bg block-bg">
    <div class="container account_container">
      <div class="row">
          <div class="col-xs-12">
            <h4 class="head_text">My Account</h4>
          </div>
      </div>
    </div>
  </article>
  
  <section id="my-account"> <!-- /# my-account area Start -->
  <div class="col-xs-12">
    <div class="row">
      <div class="col-md-3 col-sm-3 account-left">
        <ul>
        <li><a href="account-overview.php">Overview</a></li>
        <li><a href="account-edit-profile.php" class="active">Edit Profile</a></li>
        <li><a href="account-manage-address.php">My Addresses</a></li>
        <li><a href="account-manage-order.php">My Orders</a></li>
        <li><a href="account-wishlist.php">My Wishlist</a></li>
        </ul>
      </div>
      <div class="col-md-8 col-sm-8 account-right">
       <h4>EDIT PROFILE</h4>
        <div class="profile-border">
            <div class="row account-body padding15">
            <div class="col-sm-7">
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR FIRST NAME*" name="" class="custome-input">
                      </div>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR LAST NAME " name="" class="custome-input">
                      </div>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR EMAIL *" name="" class="custome-input">
                      </div>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR TELEPHONE *" name="" class="custome-input">
                      </div>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR PASSWORD *" name="" class="custome-input">
                      </div>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR PASSWORD *" name="" class="custome-input">
                      </div>
                      
                      <a href="payment-page.php" style="color:#fff; text-decoration:none; font-size:18px;" class="red-btn">SAVE</a> </div>
            </div>
        </div>
        
        
        
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="clearfix"></div>
</section>

<!-- /# my-account area End --> 
</section>
<?php include('footer.php');?>