<?php include('header.php');?>
<section class="content_part">
<!-- # list banner start-->
<section id="list-banner">
  <div class="container">
    <div class="text-content">
      <h1>IKAT - ODISHA</h1>
      <p>In ikat the resist is formed by binding individual yarns or bundles of yarns with
        a tight wrapping applied in the desired pattern. The yarns are then dyed.</p>
      <p> The bindings may then be altered to create a new pattern and the yarns dyed
        again with another colour. This process may be repeated multiple times to
        produce elaborate, multicolored patterns. </p>
      <div class="content-link"> <a href="#">BATIK</a> <a href="#">PHULKARI</a> <a href="#">BADOHI</a> </div>
    </div>
  </div>
</section>
<!-- /# list banner end -->
<section id="filter-area"> <!-- /# list filter Start -->
  <div class="container container-details3">
  <div class="col-sm-12 paddingL0">
    <div class="form-fliter">
      <label>21 STYLES FOUND</label>
    </div>
    <div class="form-fliter">
      <label>SORT BY :</label>
      <select class="m-filter-dropdown selectBox">
        <option value="">Popular</option>
        <option value="">123</option>
      </select>
    </div>
  </div>
  <!--<div class="col-sm-3">
    <div class="filter-right"><a href="#"><< Prev </a> | <a href="#" class="">Next >></a></div>
  </div>-->
</section>
<!-- /# list filter END -->
<section id="list-product"> <!-- /# list product area Start -->
  <div class="col-xs-12">
    <div class="row">
      <div class="col-md-3 col-sm-3 left-fliter">
        <div class="filter-1">
          <h3>CATEGORIES</h3>
          <div class="category-check">
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Category 1 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Category 2 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Category 3 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Category 4 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Category 5 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Category 6 </label>
            </div>
          </div>
        </div>
        <div class="filter-1">
          <h3>ArtForm</h3>
          <div class="category-check">
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> ArtForm 1 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> ArtForm 2 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> ArtForm 3 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> ArtForm 4 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> ArtForm 5 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> ArtForm 6 </label>
            </div>
          </div>
        </div>
        <div class="filter-1">
          <h3>LOCATION</h3>
          <div class="category-check">
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Location 1 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Location 2 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Location 3 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Location 4 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Location 5 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> Location 6 </label>
            </div>
          </div>
        </div>
        <div class="filter-1">
          <h3>SIZE</h3>
          <div class="checkbox-size">
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> 6 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> 8 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> 10 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> 12 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> 14 </label>
            </div>
            <div class="filters">
              <label>
                <input type="checkbox">
                <span class="icon"><i class="fa fa-check"></i></span> 16 </label>
            </div>
          </div>
        </div>
        <!--<div class="filter-1">
          <h3>COLOR</h3>
          <div class="clearfix"></div>
          <div class="multi-color"> 
          <span></span><span></span><span></span><span></span><span></span><span></span><span></span> <span></span><span></span><span></span><span></span><span></span><span></span><span></span> <span></span><span></span><span></span><span></span><span></span><span></span><span></span> <span></span><span></span><span></span><span></span><span></span><span></span><span></span> 
          </div>
        </div>-->
        <div class="filter-1">
          <h3>PRICE</h3>
          <div class="clearfix"></div>
          <div class="price-range">
            <div id="slider-range"></div>
            <p>
              <input type="text" id="amount" readonly style="border:0; color:#555656; font-size:16px; font-weight:normal; background:transparent;">
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-8 col-sm-8 right-list">
        <div class="col-xs-12 col-sm-6 col-md-5 custom-padd"> 
        <a href="product-details.php"><img src="img/products/product-10.jpg" alt="" class="img-responsive" /></a>
          <div class="img-des">Lorem Ipsum is simply dummy text <br/>
            <span>Rs. 11,000</span></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5 custom-padd"> 
        <a href="product-details.php"><img src="img/products/product-02.jpg" alt="" class="img-responsive" /></a>
          <div class="img-des">Lorem Ipsum is simply dummy text <br/>
            <span>Rs. 11,000</span></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5 custom-padd"> 
        <a href="product-details.php"><img src="img/products/product-11.jpg" alt="" class="img-responsive" /></a>
          <div class="img-des">Lorem Ipsum is simply dummy text <br/>
            <span>Rs. 11,000</span></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5 custom-padd"> 
        <a href="product-details.php"><img src="img/products/product-04.jpg" alt="" class="img-responsive" /></a>
          <div class="img-des">Lorem Ipsum is simply dummy text <br/>
            <span>Rs. 11,000</span></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5 custom-padd"> 
        <a href="product-details.php"><img src="img/products/product-01.jpg" alt="" class="img-responsive" /></a>
          <div class="img-des">Lorem Ipsum is simply dummy text <br/>
            <span>Rs. 11,000</span></div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-5 custom-padd"> 
        <a href="product-details.php"><img src="img/products/product-03.jpg" alt="" class="img-responsive" /></a>
          <div class="img-des">Lorem Ipsum is simply dummy text <br/>
            <span>Rs. 11,000</span></div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="clearfix"></div>
</section>
<!-- /# list product area End --> 
</section>
<?php include('footer.php');?>