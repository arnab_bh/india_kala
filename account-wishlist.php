<?php include('header.php');?>
<link href="css/my-account.css" rel="stylesheet"/>
<section class="content_part">
<article>
  	<div class="container account_container">
        <div class="row"> 
          <!-- Breadcrumb Column -->
          <div class="col-xs-12">
            <ol class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">My Account</li>
            </ol>
          </div>
          <!-- End Column -->
        </div>
    </div>  
  </article>
<article class="full-black-bg block-bg">
    <div class="container account_container">
      <div class="row">
          <div class="col-xs-12">
            <h4 class="head_text">My Account</h4>
          </div>
      </div>
    </div>
  </article>
  
  <section id="my-account"> <!-- /# my-account area Start -->
  <div class="col-xs-12">
    <div class="row">
      <div class="col-md-3 col-sm-3 account-left">
        <ul>
        <li><a href="account-overview.php">Overview</a></li>
        <li><a href="account-edit-profile.php">Edit Profile</a></li>
        <li><a href="account-manage-address.php">My Addresses</a></li>
        <li><a href="account-manage-order.php">My Orders</a></li>
        <li><a href="account-wishlist.php" class="active">My Wishlist</a></li>
        </ul>
      </div>
      <div class="col-md-8 col-sm-8 account-right">
       <h4>MY WISHLIST</h4>
        <div class="col-xs-12 paddingLR0">
        <div class="table-responsive">
          <table class="table table-bordered order-wishlist">
            <tbody>
              <tr>
                <th style="text-align:left;" class="padding15" scope="row">PRODUCT NAME</th>
                <th class="padding15" style="text-align:center;">QUANTITY</th>
                <th class="padding15"  style="text-align:center;">SUBTOTAL</th>
                <th  class="padding15">&nbsp;</th>
              </tr>
              <tr>
                <th>
                <div class="order-img"><img src="img/wishlist-img.jpg" alt=""/></div>
                <div class="order-des"><p class="pro-name">Black and white dust <br/> sweater dress </p>
                <p class="pro-size">Color : black <br/> Size : XS</p>
                <a href="cart.php" class="cart-btn wish-btn">Add to Bag </a>
                </div>
                </th>
                <th><input type="text" name="" placeholder="1" class="input-cart"></th>
                <th>$198</th>
                <th><a href="#">X</a></th>
              </tr>
              <tr>
                <th>
                <div class="order-img"><img src="img/wishlist-img02.jpg" alt=""/></div>
                <div class="order-des"><p class="pro-name">Black and white dust <br/> sweater dress </p>
                <p class="pro-size">Color : black <br/> Size : XS</p>
                <a href="cart.php" class="cart-btn wish-btn">Add to Bag </a>
                </div>
                </th>
                <th><input type="text" name="" placeholder="1" class="input-cart"></th>
                <th>$198</th>
                <th><a href="#">X</a></th>
              </tr>
              
              
              
              
            </tbody>
          </table>
        </div>
        
        
        
        
      </div>
        
        
        
        
        
        
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="clearfix"></div>
</section>

<!-- /# my-account area End --> 
</section>
<?php include('footer.php');?>