<?php include('header.php');?>
<link href="css/my-account.css" rel="stylesheet"/>
<section class="content_part">
<article>
  	<div class="container account_container">
        <div class="row"> 
          <!-- Breadcrumb Column -->
          <div class="col-xs-12">
            <ol class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">My Account</li>
            </ol>
          </div>
          <!-- End Column -->
        </div>
    </div>  
  </article>
<article class="full-black-bg block-bg">
    <div class="container account_container">
      <div class="row">
          <div class="col-xs-12">
            <h4 class="head_text">My Account</h4>
          </div>
      </div>
    </div>
  </article>
  
  <section id="my-account"> <!-- /# my-account area Start -->
  <div class="col-xs-12">
    <div class="row">
      <div class="col-md-3 col-sm-3 account-left">
        <ul>
        <li><a href="account-overview.php">Overview</a></li>
        <li><a href="account-edit-profile.php">Edit Profile</a></li>
        <li><a href="account-manage-address.php">My Addresses</a></li>
        <li><a href="account-manage-order.php" class="active">My Orders</a></li>
        <li><a href="account-wishlist.php">My Wishlist</a></li>
        </ul>
      </div>
      <div class="col-md-8 col-sm-8 account-right">
       <h4>MY ORDERS</h4>
        <div class="profile-border">
        	<div class="border-head">
            <p>ORDER ID #9122 </p>
            </div>
            <div class="row account-body">
            <div class="col-xs-12">
        <div class="table-responsive account-order">
          <table class="table table-bordered">
            <tbody>
              <tr>
              <th  class="padding15">&nbsp;</th>
                <th style="text-align:left;" class="padding15" scope="row">PRODUCT NAME</th>
                <th class="padding15" style="text-align:center;">QUANTITY</th>
                <th class="padding15"  style="text-align:center;">SUBTOTAL</th>
              </tr>
              <tr>
              <th style="text-align:center; font-size:17px;" class="border0">1</th>
                <th class="border0">
                <div class="order-img"><img src="img/order-img.jpg" alt=""/></div>
                <div class="order-des"><p class="pro-name">Black and white dust <br/> sweater dress </p>
                <p class="pro-size">Color : black <br/> Size : XS</p>
                </div>
                </th>
                <th  class="border0"><input type="text" name="" placeholder="1" class="input-cart"></th>
                <th  class="border0" style="text-align:center; font-size:17px;">$198</th>
              </tr>
              <tr>
              <th style="text-align:center; font-size:17px;" class="border0">2</th>
                <th class="border0">
                <div class="order-img"><img src="img/order-img.jpg" alt=""/></div>
                <div class="order-des"><p class="pro-name">Black and white dust <br/> sweater dress </p>
                <p class="pro-size">Color : black <br/> Size : XS</p>
                </div>
                </th>
                <th  class="border0"><input type="text" name="" placeholder="1" class="input-cart"></th>
                <th  class="border0" style="text-align:center; font-size:17px;">$198</th>
              </tr>
              
              <tr class="order-total">
              <th class="border0">&nbsp;</th>
                <th class="border0">&nbsp;</th>
                <th  class="border0" style="text-align:left; font-weight:normal; font-size:18px; width:26%; color:#676767;">TOTAL : </th>
                <th  class="border0" style="text-align:center; font-size:18px; color:#cf4847;">$780</th>
              </tr>
              
            </tbody>
          </table>
        </div>
        
        <div class="order-details">
        	<div class="date-area">
            <p><strong>SHIPPING DATE :</strong>  01/01/2016</p>
            <p><strong>PAYMENT MODE :</strong> Cash On Delivery</p>
            </div>
            <div class="mode-area">
            <p><strong>VOUCHER :</strong>  01/01/2016</p>
            <p><strong>CATEGORY :</strong> Category</p>
            </div>
            <div class="return-area">
            <a href="cart.php" class="cart-btn">RETURN </a>
            </div>
        
        </div>
        
        
      </div>
            </div>
        </div>
        
        
        <div class="profile-border">
        	<div class="border-head">
            <p>ORDER ID #9122 </p>
            </div>
            <div class="row account-body">
            <div class="col-xs-12">
        <div class="table-responsive account-order">
          <table class="table table-bordered">
            <tbody>
              <tr>
              <th  class="padding15">&nbsp;</th>
                <th style="text-align:left;" class="padding15" scope="row">PRODUCT NAME</th>
                <th class="padding15" style="text-align:center;">QUANTITY</th>
                <th class="padding15"  style="text-align:center;">SUBTOTAL</th>
              </tr>
              <tr>
              <th style="text-align:center; font-size:17px;" class="border0">1</th>
                <th class="border0">
                <div class="order-img"><img src="img/order-img.jpg" alt=""/></div>
                <div class="order-des"><p class="pro-name">Black and white dust <br/> sweater dress </p>
                <p class="pro-size">Color : black <br/> Size : XS</p>
                </div>
                </th>
                <th  class="border0"><input type="text" name="" placeholder="1" class="input-cart"></th>
                <th  class="border0" style="text-align:center; font-size:17px;">$198</th>
              </tr>
              <tr>
              <th style="text-align:center; font-size:17px;" class="border0">2</th>
                <th class="border0">
                <div class="order-img"><img src="img/order-img.jpg" alt=""/></div>
                <div class="order-des"><p class="pro-name">Black and white dust <br/> sweater dress </p>
                <p class="pro-size">Color : black <br/> Size : XS</p>
                </div>
                </th>
                <th  class="border0"><input type="text" name="" placeholder="1" class="input-cart"></th>
                <th  class="border0" style="text-align:center; font-size:17px;">$198</th>
              </tr>
              
              <tr class="order-total">
              <th class="border0">&nbsp;</th>
                <th class="border0">&nbsp;</th>
                <th  class="border0" style="text-align:left; font-weight:normal; font-size:18px; width:26%; color:#676767;">TOTAL : </th>
                <th  class="border0" style="text-align:center; font-size:18px; color:#cf4847;">$780</th>
              </tr>
              
            </tbody>
          </table>
        </div>
        
        <div class="order-details">
        	<div class="date-area">
            <p><strong>SHIPPING DATE :</strong>  01/01/2016</p>
            <p><strong>PAYMENT MODE :</strong> Cash On Delivery</p>
            </div>
            <div class="mode-area">
            <p><strong>VOUCHER :</strong>  01/01/2016</p>
            <p><strong>CATEGORY :</strong> Category</p>
            </div>
            <div class="return-area">
            <a href="cart.php" class="cart-btn">RETURN </a>
            </div>
        
        </div>
        
        
      </div>
            </div>
        </div>
        
        
        
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="clearfix"></div>
</section>

<!-- /# my-account area End --> 
</section>
<?php include('footer.php');?>