<?php include('header.php');?>
<section class="content_part">
<!-- # product details top start-->
<section id="product-details">
  <div class="container container-details">
    <div class="row"> 
      <!-- Breadcrumb Column -->
      <div class="col-xs-12">
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li class="active">Shopping Cart</li>
        </ol>
      </div>
    </div>
  </div>
  <!-- End Column -->
  <div class="full-black-bg">
    <div class="container">
      <div class="table-responsive ">
        <table class="table">
          <thead >
            <tr>
              <th class="green-bg width01">PRODUCT NAME</th>
              <th class="green-bg width02">&nbsp;</th>
              <th class="green-bg width03">UNIT PRICE</th>
              <th class="green-bg width03">QUANTITY</th>
              <th class="green-bg width03">SUBTOTAL</th>
              <th class="green-bg">&nbsp;</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="table-responsive cart-table">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th style="border-right:0;" class="width01" scope="row"><img src="img/products/thum-img-01.jpg" alt=""/></th>
                <td style="border-left:0;" class="width02"><p class="pro-name">Black and white dust <br/>
                    Sweater dress</p>
                  <p class="pro-size">Color: black <br/>
                    Size: XS<br/>
                    Shipping Time: November 12, 2016</p></td>
                <td class="width03"><span class="over-line">Rs. 321</span> Rs. 198</td>
                <td class="width03"><input type="text" name="" placeholder="1" class="input-cart"></td>
                <td class="width03"><span class="cart-tp">Rs. 198</span></td>
                <td style="width:4%"><a href="javascript void(0)" class="cart-remove">x</a></td>
              </tr>
              <tr>
                <th style="border-right:0;" class="width01" scope="row"><img src="img/products/thum-img-02.jpg" alt=""/></th>
                <td style="border-left:0;" class="width02"><p class="pro-name">Pale pink and black <br/>
                    buttoned dress </p>
                  <p class="pro-size">Color: black <br/>
                    Size: XS<br/>
                    Shipping Time: November 12, 2016</p></td>
                <td class="width03"><span class="over-line">Rs. 457</span> Rs. 215</td>
                <td class="width03"><input type="text" name="" placeholder="1" class="input-cart"></td>
                <td class="width03"><span class="cart-tp">Rs. 215</span></td>
                <td style="width:4%"><a href="javascript void(0)" class="cart-remove">x</a></td>
              </tr>
              <tr>
                <th style="border-right:0;" class="width01" scope="row"><img src="img/products/thum-img-03.jpg" alt=""/></th>
                <td style="border-left:0;" class="width02"><p class="pro-name">Black puplum waist-tie <br/>
                    kududress </p>
                  <p class="pro-size">Color: black <br/>
                    Size: XS<br/>
                    Shipping Time: November 12, 2016</p></td>
                <td class="width03"> Rs. 324</td>
                <td class="width03"><input type="text" name="" placeholder="1" class="input-cart"></td>
                <td class="width03"><span class="cart-tp">Rs. 324</span></td>
                <td style="width:4%"><a href="javascript void(0)" class="cart-remove">x</a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
      	<div class="">
          
          <div class="col-sm-4 L-mob paddingR0 pull-right" >
            <div class="table-responsive cart-total table-bordered">
              <table class="table table-bordered">
                <tr>
                  <td style="border-top:none; color:#676767;">SUBTOTAL :</td>
                  <td style="border-right:none;border-top:none;">Rs. 737</td>
                </tr>
                <tr>
                  <td style="color:#676767;">SHIPPING : </td>
                  <td style="border-right:none;">Rs. 6</td>
                </tr>
                <tr>
                  <td style="color:#676767;">TAX(5%) :</td>
                  <td style="border-right:none;">Rs. 37</td>
                </tr>
                <tr>
                  <td style="border-bottom:none;color:#676767;">TOTAL :</td>
                  <td style="border-right:none;border-bottom:none;color:#cf4847">Rs. 780</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-xs-12"> <a href="product-list.php" class="con-btn">CONTINUE SHOPPING</a> 
      <a href="checkout.php" class="check-btn">CHECKOUT</a> </div>
      <div class="clearfix"></div>
      <!-- End Column --> 
    </div>
  </div>
</section>
<!-- /# product details top end --> 
</section>
<!-- footer Part Added-->
<?php include('footer.php');?>