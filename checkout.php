<?php include('header.php');?>
<link rel="stylesheet" type="text/css" href="css/payment_option.css" >
<section class="content_part">
<!-- # product details top start-->
  <article>
  	<div class="container container-details">
        <div class="row"> 
          <!-- Breadcrumb Column -->
          <div class="col-xs-12">
            <ol class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">Checkout</li>
            </ol>
          </div>
          <!-- End Column -->
        </div>
    </div>  
  </article>
  
  <article class="full-black-bg">
    <div class="container payment_container">
      <div class="row">
          <div class="col-xs-12">
            <h4 class="head_text">STEP 1 : BILLING INFORMATION</h4>
          </div>
      </div>
    </div>
  </article>
  
  <article>
  	<div class="container payment_container">
      <div class="row">
          <div class="login-area">
                    <div class="col-sm-6">
                      <h4>YOUR PERSONAL DETAILS</h4>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR NAME *" name="" class="custome-input"/>
                      </div>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR EMAIL *" name="" class="custome-input"/>
                      </div>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR CONTACT NUMBER *" name="" class="custome-input"/>
                      </div>
                      
                      <!--<div class="filters">
                        <label>
                          <input type="checkbox">
                          <span class="icon"><i class="fa fa-check"></i></span> I wish to subscribe to the Vigo Shop newsletter. </label>
                      </div>-->
                      <div class="filters">
                        <label>
                          <input type="checkbox">
                          <span class="icon"><i class="fa fa-check"></i></span> My delivery and billing address are the same. </label>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <h4>YOUR ADDRESS</h4>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR FlAT NO. BUILDING NAME*" name="" class="custome-input"/>
                      </div>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR STREET NAME " name="" class="custome-input"/>
                      </div>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR CITY *" name="" class="custome-input"/>
                      </div>
                      <div class="form-group">
                        <input type="text" placeholder="ENTER YOUR POSTCODE *" name="" class="custome-input"/>
                      </div>
                      <div class="form-group">
                        <select name="" class="custome-input">
                          <option>ENTER YOUR POST COUNTRY *</option>
                          <option>India</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <select name="" class="custome-input">
                          <option>ENTER YOUR POST REGION/STATE * </option>
                          <option>India</option>
                        </select>
                      </div>
                      <div class="filters">
                        <label>
                          <input type="checkbox">
                          <span class="icon"><i class="fa fa-check"></i></span> I have reed and agree to the Privacy Policy. </label>
                      </div>
                      <a href="payment-page.php" class="red-btn">Continue</a> </div>
                  </div>
      </div>
    </div>
  </article>
<!-- /# product details top end --> 
</section>
<!-- footer Part Added-->
<?php include('footer.php');?>