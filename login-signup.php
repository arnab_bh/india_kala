<?php include('header.php');?>
<section class="content_part"> 
  <!-- /login area start -->
  <div class="container container-details">
    <div class="col-xs-12">
      <ol class="breadcrumb">
        <li><a href="">Home</a></li>
        <li class="active">Register</li>
      </ol>
    </div>
  </div>
  <div class="question-heading">
    <div class="container">
      <p class="indiakala-questext">DO YOU HAVE A INDIAKALA ACCOUNT ?</p>
    </div>
  </div>
  <section class="login-style">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="text-mid fom-L0">
            <p class="why-course">WHY YES, YES OF COURSE</p>
            <form role="form">
              <div class="form-group">
                <label for="" class="form-text">EMAIL ADDRESS</label>
                <input type="email" class="form-control form-n-cont" id="email">
              </div>
              <div class="form-group">
                <label for="" class="form-text">PASSWORD</label>
                <input type="password" class="form-control form-n-cont" id="pwd">
                <p class="forget-pass">FORGET PASSWORD</p>
              </div>
              <div class="filters">
                <label>
                  <input type="checkbox">
                  <span class="icon"><i class="fa fa-check"></i></span> KEEP ME LOGGED IN</label>
              </div>
            </form>
          </div>
        </div>
        <div class="col-sm-4" style="margin-top:46px;">
          <p class="why-course01">IF NOT,YOU SHOULD</p>
          <P class="test-para">CREATE A INDIAKALA ACCOUNT TO ENJOY:</P>
          <P class="test-para">- FASTER CHECKOUT WITHOUT SAVE DETAILS</P>
          <P class="test-para">- EASY ORDER TRACKING AND ORDER HISTORIES</P>
          <P class="test-para">- SHARE YOUR FAVORITE PRODUCTS VIA WISHLIST</P>
          <button type="button" class="btn btn-default button01" id="SignUp">CREATE AN ACCOUNT</button>
      	</div>
        <div class="col-sm-4">
          <p class="login">LOGIN WITH</p>
          <button type="button" class="btn btn-default button02"><i class="fa fa-facebook"></i> Connect with facebook</button>
          <button type="button" class="btn btn-default button03"><i class="fa fa-google-plus"></i> Connect with google </button>
        </div>
    </div>
  </section>
  <section class="login-style" id="sign_sec">
  <div class="question-heading">
    <div class="container">
      <p class="indiakala-questext">LET ' S GET TO KNOW TO EACH OTHER</p>
    </div>
  </div>
  <div class="container cont-MTOp">
      <div class="row">
        <div class="col-sm-12">
            <form role="form">
              <div class="form-group col-sm-4">
                <label for="" class="form-text">NAME*</label>
                <input type="test" class="form-control form-n-cont" id="">
              </div>
              <div class="form-group col-sm-4">
                <label for="" class="form-text">EMAIL ID*</label>
                <input type="text" class="form-control form-n-cont" id="">
              </div>
              <div class="form-group col-sm-4">
                <label for="" class="form-text">CONTACT NUMBER*</label>
                <input type="password" class="form-control form-n-cont" id="">
              </div>
            </form>
        </div>
        <div class="col-sm-12"><button type="button" class="btn btn-default btn-button04" onclick="window.location.href='index.php'">SUBMIT</button></div>
        <!--<div class="text-mid">
          <P class="text-latest-form">GET THE LATEST FORM HARVEYNICHOLS STRAIGHT TO YOUR INBOX</P>
          <label class="radio-inline">
            <input type="radio" name="optradio">
            Yes Please</label>
          <label class="radio-inline">
            <input type="radio" name="optradio">
            No thanks</label>
          <p class="col-sm-10 sign-up-text">By signing up, you agree to receive direct marketing from INDIAKALA, INDIAKALA does not share your email or personal details with anyone else. For more information please see our privace police.you will be able to unsubcribe from all iNDIAKALA email communications at any time by clicking on the unsubscribe link in any email we send to you</p>
        </div>-->
      </div>
      
    </div>
  </section>
  
  <!-- /login area END --> 
</section>
<?php include('footer.php');?>
<script>
	$('#SignUp').click(function(){
		$('#sign_sec').css('display','block');
		});
SignUp
</script>