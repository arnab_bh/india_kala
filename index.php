<?php include('header.php');?>
<link rel="stylesheet" href="css/home-slider.css">
<!-- Header Part Added-->
 
<section class="content_part">
<!-- #banner -->
<article id="home-banner">
  <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="3000" id="bs-carousel"> 
    <!-- Overlay --> 
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
      <li data-target="#bs-carousel" data-slide-to="1"></li>
      <li data-target="#bs-carousel" data-slide-to="2"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active"> <img class="img-responsive" src="img/slides/image2.jpg" alt="slide-1"/>
        <div class="hero">
          <hgroup>
            <h1>Women</h1>
            <h3>check your autumn style</h3>
            <p>Vivamus lacinia urna lorem, eget laoreet mauris lobortis quis. Aliquam<br/>
              aliquet nec tempor a, dapibus vitae nunc. Maecenas vitae purus<br/>
              sem quis, varius tortor.</p>
          </hgroup>
          <a class="btn btn-hero btn-lg" href="product-list.php">SHOP NOW</a>
        </div>
      </div>
      <div class="item"> <img class="img-responsive" src="img/slides/image2.jpg" alt="slide-2">
        <div class="hero">
          <hgroup>
            <h1>Women</h1>
            <h3>check your autumn style</h3>
            <p>Vivamus lacinia urna lorem, eget laoreet mauris lobortis quis. Aliquam<br/>
              aliquet nec tempor a, dapibus vitae nunc. Maecenas vitae purus<br/>
              sem quis, varius tortor.</p>
          </hgroup>
          <a class="btn btn-hero btn-lg" href="product-list.php">SHOP NOW</a>
        </div>
      </div>
      <div class="item"> <img class="img-responsive" src="img/slides/image2.jpg" alt="slide-3">
        <div class="hero">
          <hgroup>
            <h1>Women</h1>
            <h3>check your autumn style</h3>
            <p>Vivamus lacinia urna lorem, eget laoreet mauris lobortis quis. Aliquam<br/>
              aliquet nec tempor a, dapibus vitae nunc. Maecenas vitae purus<br/>
              sem quis, varius tortor.</p>
          </hgroup>
          <a class="btn btn-hero btn-lg" href="product-list.php">SHOP NOW</a>
        </div>
      </div>
    </div>
  </div>
</article>
<!-- /#banner --> 
<!-- #trending items -->
<article id="our-projects">
  <div class="section-title">
    <h1><span class="section_ab1">TRENDING ITEMS</span></h1>
    <p>Explore shoppers' top finds from around the marketplace.</p>
  </div>
  <div class="container">
    <div class="col-sm-12 home-gallery">
      <div class="div-left">
        <div class="left-col">
          <div class="imageHolder"> <img src="img/our-projects/trending1.jpg" class="img-responsive" />
            <div class="caption">
              <div class="text-desc"> Some Dummy Tote<br/>
                <span>Sofia Banati</span> </div>
              <!--<div class="desc-icon"> <span><i class="fa fa-heart"></i></span> </div>-->
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="right-col">
          <div class="imageHolder"> <img src="img/our-projects/trending2.jpg" class="img-responsive" />
            <div class="caption">
              <div class="text-desc"> Some Dummy Tote<br/>
                <span>Sofia Banati</span> </div>
              <!--<div class="desc-icon"> <span><i class="fa fa-heart"></i></span> </div>-->
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="right-col rightMar">
          <div class="imageHolder"> <img src="img/our-projects/trending2.jpg" class="img-responsive" />
            <div class="caption">
              <div class="text-desc"> Some Dummy Tote<br/>
                <span>Sofia Banati</span> </div>
              <!--<div class="desc-icon"> <span><i class="fa fa-heart"></i></span> </div>-->
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <div class="left-col leftMar">
          <div class="imageHolder"> <img src="img/our-projects/trending1.jpg" class="img-responsive" />
            <div class="caption">
              <div class="text-desc"> Some Dummy Tote<br/>
                <span>Sofia Banati</span> </div>
              <!--<div class="desc-icon"> <span><i class="fa fa-heart"></i></span> </div>-->
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="div-right">
        <div class="imageHolder"> <img src="img/our-projects/trending3.jpg" class="img-responsive" />
          <div class="caption">
            <div class="text-desc"> Some Dummy Tote<br/>
              <span>Sofia Banati</span> </div>
            <!--<div class="desc-icon"> <span><i class="fa fa-heart"></i></span> </div>-->
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>
<!-- /#trending items -->
<!-- #features -->
<article id="testimonials">
  <div class="section-title">
    <h1><span class="section_ab1">our features</span></h1>
    <p>Explore shoppers' top finds from around the marketplace.</p>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-6 wow bounceInLeft hvr-float-shadow LRpadding">
        <div class="block">
          <div class="icon_block"> <img src="img/testimonial/original.png"> </div>
          <p class="fon-t2">Authenticity , Ethically Sourced, 100% Product Guarantee</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 hvr-float-shadow LRpadding">
        <div class="block2">
          <div class="icon_block"> <img src="img/testimonial/return.png"> </div>
          <p class="fon-t2">100% Returns Policy</p>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-6 col-lg-offset-0 col-md-offset-0 col-sm-offset-3 wow bounceInRight hvr-float-shadow LRpadding">
        <div class="block3">
          <p class="fon-t2">"We love the idea of taking vintage in, giving it new life, then selling it to someone who is going 
          to enjoy it for many years to come."</p>
          <h3 class="extra_abc">JAmes dOE</h3>
        </div>
      </div>
    </div>
  </div>
</article>
<!-- /#features --> 
<!-- #our-stories -->
<article id="our-stories" >
  <div class="section-title">
    <h1><span class="section_ab1">our stories</span></h1>
    <p>Some Normal Dummy text to bring out the expression.</p>
  </div>
  <div class="clearfix"></div>
  <div class="stories-bg">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 storiesw-1"> <img src="img/agra-img.jpg" alt=""/>
          <div class="agra-desc"> THE TRADE FAIR <span>AGRA <i class="fa fa-chevron-right"></i></span> </div>
        </div>
        <div class="col-sm-2 storiesw-2">
          <h3>THE <br/>
            BATIK<br/>
            BOYS</h3>
          <p>Lcendis consequun<br/>
            tur.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis consequuntur.Lorem ipsum dolor sit amet, 
            consectetur adipisicing elit. Reiciendis consequuntur.Lorem ipsum dolor sit amet,
            Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, </p>
          <div class="clearfix"></div>
          <a href="#" class="read_more">READ MORE</a> </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</article>
<!-- /#our-stories -->
</section>
<!-- footer Part Added-->
<?php include('footer.php');?>