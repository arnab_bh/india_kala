<?php include('header.php');?>
<link rel="stylesheet" href="css/easyzoom.css" type="text/css" media="all"> 
<!-- Header Part Added-->
<section class="content_part">
<!-- # product details top start-->
<article id="product-details">
  <div class="container container-details">
    <div class="row"> 
      <!-- Breadcrumb Column -->
      <div class="col-xs-12">
        <ol class="breadcrumb">
          <li><a href="index.php">Home</a></li>
          <li><a href="javascript void(0)">Fashion</a></li>
          <li class="active">Shetland Wool Full - Zip Sweater</li>
        </ol>
      </div>
      <!-- End Column -->
      <div class="col-xs-12">
      <div class="row">
        <div class="col-sm-7 mob-padding">
          <div class="row">
            <div class="col-sm-12">
              <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails"> <a href="img/products/product-02_zoom.jpg"> 
              <img src="img/products/product-02_big.jpg" alt="" width="100%" height="" /> </a> </div>
            </div>
            <div class="col-sm-12">
              <ul class="thumbnails">
                <li> <a href="img/products/product-02_zoom.jpg" data-standard="img/products/product-02_big.jpg"> <img src="img/products/thum-img-02.jpg" alt="" /> </a> </li>
                <li> <a href="img/products/product-98_zoom.jpg" data-standard="img/products/product-98_big.jpg"> <img src="img/products/thum-img-98.jpg" alt="" /> </a> </li>
                <li> <a href="img/products/product-99_zoom.jpg" data-standard="img/products/product-99_big.jpg"> <img src="img/products/thum-img-99.jpg" alt="" /> </a> </li>
                <li> <a href="img/products/product-01_zoom.jpg" data-standard="img/products/product-01_big.jpg"> <img src="img/products/thum-img-01.jpg" alt="" /> </a> </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-5 mob-padding">
          <div class="poduct-name">
            <h2>SHETLAND WOOL FULL - ZIP SWEATER</h2>
            <p><span>Rs. 180</span> Rs. 110</p>
          </div>
          <div class="stock-area">
            <p> Availability : in Stock </p>
            <p> Product Code : 483512569 </p>
            <p> Brand : Apple</p>
          </div>
          <div class="select-area">
            <p>SELECT SIZE</p>
            <a href="javascript void(0)">S</a><a href="#">M</a><a href="#" class="active">L</a><a href="#">XL</a> </div>
          <!--<div class="select-color">
            <p>SELECT COLOR</p>
            <a href="javascript void(0)"><img src="img/color-01.jpg" alt=""/></a> <a href="#"><img src="img/color-02.jpg" alt=""/></a> 
            <a href="javascript void(0)"><img src="img/color-03.jpg" alt=""/></a> <a href="#"><img src="img/color-04.jpg" alt=""/></a> 
            <a href="javascript void(0)"><img src="img/color-05.jpg" alt=""/></a> </div>-->
          <div class="cart-area">
            <select class="input-change">
            	<option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </select>
            <a href="cart.php" class="cart-btn">Add to Bag </a>
            <div class="cart-right-link">
              
              <a href="javascript void(0)" class="mTop6">+Add to Wishlist</a>
              <!--<a href="javascript void(0)">+Add to Compare </a> </div>-->
              <div class="clearfix"></div>
          </div>
          <!--<div class="share-icon">
            <p>Share</p>
            <a href="#" class="fb"><i class="fa fa-facebook"></i></a> <a href="#" class="tu" ><i class="fa fa-tumblr"></i></a> 
            <a href="#" class="en"><i class="fa fa-envelope"></i></a> <a href="#" class="li"><i class="fa fa-linkedin"></i></a>
            <a href="#" class="gp"><i class="fa fa-google-plus"></i></a> </div>-->
        </div>
      </div>
      </div>
      <!-- End Column --> 
    </div>
  </div>
</article>
<!-- /# product details top end -->
<article id="product-content"> <!-- /# list product area Start -->
  <div class="section-title">
    <h1><span class="section_ab1">THE PRODUCT</span></h1>
  </div>
  <div class="product-gray-bg">
    <div class="container container-details2">
      <div class="row">
        <div class="col-sm-12 right-img-text">
          <p></p>
          <p><img src="img/right-content-img.jpg" class="right-content-img" alt=""/>In <strong>ikat</strong> the resist is formed by binding 
          individual yarns or bundles of yarns with a tight wrapping applied in the desired pattern. The yarns are then dyed.</p>
          <p>The bindings may <strong>then be altered to create a new pattern and the yarns dyed again with another colour.</strong> This process 
          may be repeated multiple times to produce elaborate, <strong>multicolored patterns.</strong> </p>
          <p>When the dyeing is finished all the bindings are removed and the yarns are woven into cloth. In <strong>other resist-dyeing 
          techniques</strong> such as tie-dye and <strong>batik the resist is applied</strong> to the woven cloth, whereas in ikat teh  resist 
          is applied to the yarns before they are woven into cloth.</p>
        </div>
        <div class="col-sm-12 right-img-text">
          <p><img src="img/left-content-img.jpg" class="left-content-img" alt=""/>
          <p></p>
          In ikat the resist is formed by binding individual yarns or bundles of yarns with
          a tight wrapping applied in the desired pattern. The yarns are then dyed.
          </p>
          <p><strong>The bindings may then be altered to create a new pattern and the yarns dyed again with another colour.</strong> 
          This process may be repeated multiple times to <strong>produce elaborate, multicolored patterns.</strong> </p>
          <p>When the dyeing is finished all the bindings are removed and the yarns
            are woven into cloth. <strong>In other resist-dyeing techniques such as tie-dye</strong> and batik the resist is applied to 
            the woven cloth, whereas in ikat teh</p>
        </div>
      </div>
    </div>
  </div>
</article>
<!-- /# list product area End -->
<article id="list-product"> <!-- /# list product area Start -->
  <div class="section-title">
    <h1><span class="section_ab1">SIMILAR PRODUCTS</span></h1>
  </div>
  <div class="container container-details">
    <div class="row">
      <div class="col-sm-4 custom-padd"> <a href="#"><img src="img/products/similar-01.jpg" alt="" class="img-responsive" /></a>
        <div class="img-des">SOME DUMMY PRODUCT PRODUCT <br/>
          <span>Rs. 11,000</span></div>
      </div>
      <div class="col-sm-4 custom-padd"> <a href="#"><img src="img/products/similar-02.jpg" alt="" class="img-responsive" /></a>
        <div class="img-des">SOME DUMMY PRODUCT PRODUCT <br/>
          <span>Rs. 11,000</span></div>
      </div>
      <div class="col-sm-4 custom-padd"> <a href="#"><img src="img/products/similar-03.jpg" alt="" class="img-responsive" /></a>
        <div class="img-des">SOME DUMMY PRODUCT PRODUCT <br/>
          <span>Rs. 11,000</span></div>
      </div>
    </div>
  </div>
</article>
<!-- /# list product area End --> 
</section>
<!-- footer Part Added-->
<?php include('footer.php');?>
