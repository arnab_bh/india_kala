<?php include('header.php');?>
<link rel="stylesheet" type="text/css" href="css/payment_option.css" >
<section class="content_part">
<!-- # product details top start-->
  <article>
  	<div class="container container-details">
        <div class="row"> 
          <!-- Breadcrumb Column -->
          <div class="col-xs-12">
            <ol class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">Checkout</li>
            </ol>
          </div>
          <!-- End Column -->
        </div>
    </div>  
  </article>
  
  <article class="full-black-bg">
    <div class="container payment_container">
      <div class="row">
          <div class="col-xs-12">
            <h4 class="head_text">STEP 2 : confirm order</h4>
          </div>
      </div>
    </div>
  </article>
  
  <article>
  	<div class="container payment_container">
      <div class="row">
          <div class="col-xs-12 checkout-acc table-bordered paddingLR0">
             <div class="check-table">
                    <table class="table">
                      <thead>
                        <tr>
                          <th class="white-bg">&nbsp;</th>
                          <th class="white-bg">PRODUCT NAME</th>
                          <th class="white-bg">PRODUCT CODE</th>
                          <th class="white-bg">UNIT PRICE</th>
                          <th class="white-bg">QUANTITY</th>
                          <th class="white-bg">SUBTOTAL</th>
                          <th class="white-bg"><!--<div class="remove-round">x</div>--></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td style="border-right:0; padding-left:16px;" scope="row"><img src="img/cart-img-01.jpg" alt=""></td>
                          <td style="border-left:0;"><p class="pro-name">Black and white dust <br>
                              Sweater dress</p>
                            <p class="pro-size">Color: black <br>
                              Size: XS</p></td>
                          <td>MP125984154</td>
                          <td><span class="over-line">$321</span> $198</td>
                          <td><input type="text" name="" placeholder="1" class="input-cart"></td>
                          <td><span class="cart-tp">$198</span></td>
                          <td><a href="#" class="cart-remove">x</a></td>
                        </tr>
                        <tr>
                          <td style="border-right:0; padding-left:16px;" scope="row"><img src="img/cart-img-02.jpg" alt=""></td>
                          <td style="border-left:0;"><p class="pro-name">Black and white dust <br>
                              Sweater dress</p>
                            <p class="pro-size">Color: black <br>
                              Size: XS</p></td>
                          <td>MP125984154</td>
                          <td><span class="over-line">$321</span> $198</td>
                          <td><input type="text" name="" placeholder="1" class="input-cart"></td>
                          <td><span class="cart-tp">$215</span></td>
                          <td><a href="#" class="cart-remove">x</a></td>
                        </tr>
                        <tr>
                          <td style="border-right:0; padding-left:16px;" scope="row"><img src="img/cart-img-03.jpg" alt=""></td>
                          <td style="border-left:0;"><p class="pro-name">Black and white dust <br>
                              Sweater dress</p>
                            <p class="pro-size">Color: black <br>
                              Size: XS</p></td>
                          <td>MP125984154</td>
                          <td><span class="over-line">$321</span> $198</td>
                          <td><input type="text" name="" placeholder="1" class="input-cart"></td>
                          <td><span class="cart-tp">$324</span></td>
                          <td><a href="#" class="cart-remove">x</a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-sm-5 pull-right">
                  <div class="cart-total">
                    <table class="table table-bordered">
                      <tbody><tr>
                        <td>SUBTOTAL :</td>
                        <td style="border-right:none;">$737</td>
                      </tr>
                      <tr>
                        <td>SHIPPING : </td>
                        <td style="border-right:none;">$6</td>
                      </tr>
                      <tr>
                        <td>TAX(5%) :</td>
                        <td style="border-right:none;">$37</td>
                      </tr>
                      <tr>
                        <td>TOTAL :</td>
                        <td style="border-right:none; color:#cf4847;">$780</td>
                      </tr>
                    </tbody></table>
                  </div>
                </div>
          </div>
          <div class="col-xs-12"> <a href="payment-page.php" class="conf-btn" style="margin-top:56px;">CONFIRM ORDER</a> </div>
      </div>
    </div>
  </article>
<!-- /# product details top end --> 
</section>
<!-- footer Part Added-->
<?php include('footer.php');?>