<?php include('header.php');?>
<link rel="stylesheet" type="text/css" href="css/thank_you.css" >

<section class="content_part">
	<article>
  		<div class="container thank_container">
        	<div class="sucess" style="font-size:21px; margin-top:58px;">You're done</div>
            <div class="congoz" style="font-size:35px; margin-top:25px;">CONGRATULATIONS!</div>
            <div class="sucess" style="font-size:18px; margin-top:18px;">Your order was successfully placed. Thank you for shopping with us.</div>
            <div class="order_confirm">
            	<div class="sucess" style="font-size:21px;">Order confirmation no</div>
                <div class="congoz" style="font-size:35px; margin-top:15px;">WF1493</div>
            </div>
            <div class="share_area">
                <div class="congoz" style="font-size:35px; margin-bottom:25px;">SHARE IT LIKE A GROUPIE, SHOUT ABOUT IT</div>
                <div class="share_icon">
                	<ul>
                    	<li><a href="javascript void(0)"><img src="img/fb_img.png" /></a></li>
                        <li><a href="javascript void(0)"><img src="img/twit_img.png" /></a></li>
                    </ul>
                </div>  
            </div>
            <div class="success" style="font-size:21px; margin-top:40px;">Please check your registered email for more details.</div>
            <div class="order_question">Have a question? Call us on 8802-139-220</div>
        </div>
    </article>
</section>

<!-- footer Part Added-->
<?php include('footer.php');?>