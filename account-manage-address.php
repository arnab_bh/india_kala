<?php include('header.php');?>
<link href="css/my-account.css" rel="stylesheet"/>
<section class="content_part">
<article>
  	<div class="container account_container">
        <div class="row"> 
          <!-- Breadcrumb Column -->
          <div class="col-xs-12">
            <ol class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">My Account</li>
            </ol>
          </div>
          <!-- End Column -->
        </div>
    </div>  
  </article>
<article class="full-black-bg block-bg">
    <div class="container account_container">
      <div class="row">
          <div class="col-xs-12">
            <h4 class="head_text">My Account</h4>
          </div>
      </div>
    </div>
  </article>
  
  <section id="my-account"> <!-- /# my-account area Start -->
  <div class="col-xs-12">
    <div class="row">
      <div class="col-md-3 col-sm-3 account-left">
        <ul>
        <li><a href="account-overview.php">Overview</a></li>
        <li><a href="account-edit-profile.php">Edit Profile</a></li>
        <li><a href="account-manage-address.php" class="active">My Addresses</a></li>
        <li><a href="account-manage-order.php">My Orders</a></li>
        <li><a href="account-wishlist.php">My Wishlist</a></li>
        </ul>
      </div>
      <div class="col-md-8 col-sm-8 account-right">
       <h4>Manage Address</h4>
       <a href="payment-page.php" style="color:#fff; text-decoration:none; font-size:18px; margin-top:0;" class="red-btn">ADD ADDRESS</a>
        <div class="profile-border">
        	<div class="border-head">
            <p>MY ADDRESSES <a href="javascript:void(0)" class="pul-right">Manage your Addresses</a></p>
            </div>
            <div class="row account-body">
            <div class="col-sm-6">
            <h5>Default Billing Address</h5>
            <p>Mr. Vedanshu Srivastava<br/>
            Navi Mumbai,<br/>
            Maharashtra,<br/>
            Kharghar, MAHARASHTRA, 412010<br/>
            India<br/>
            T +918802139220</p>
            <a href="javascript:void(0)">Edit Billing Address</a>
            </div>
            <div class="col-sm-6">
            <h5>Default Shipping Address</h5>
            <p>Mr. Vedanshu Srivastava<br/>
            Navi Mumbai,<br/>
            Maharashtra,<br/>
            Kharghar, MAHARASHTRA, 412010<br/>
            India<br/>
            T +918802139220</p>
            <a href="javascript:void(0)">Edit Shipping Address</a>
            </div>
            </div>
        </div>
        <div class="profile-border">
        	<div class="border-head">
            <p>ADDITIONAL ADDRESS ENTRIES</p>
            </div>
            <div class="row account-body">
            <div class="col-sm-12">
            <p>You have no additional address entires in your address book</p>
            </div>
            
            </div>
        </div>
        
        
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="clearfix"></div>
</section>

<!-- /# my-account area End --> 
</section>
<?php include('footer.php');?>